This repository provide all the necessary material
for PFIA 2020 tutorial
"Formal verification of deep neural networks".

The main file is a jupyter notebook `tutorial.ipynb`,
slides for the first
part of the tutorial are under `doc/slides.pdf`.

There are several dependancies to install.
For convenience, a Docker image
is provided.

### Install the tutorial environment within a Docker image (recommended)

The Dockerfile contains all dependencies for running the
tutorial. If you want to regenerate the training data and
retrain the network by yourself, you will need to install
the `torch` python package.

During build, about 300MB of data will be downloaded.

#### Build Docker image
1. Install Docker
    1. on Windows or macOS, install Docker Desktop
    1. on Linux systems, install the Docker toolbox:
        * on Debian/Ubuntu, `sudo apt-get update && sudo apt-get install docker`
        * on Archlinux, `sudo pacman -Syu docker`
        * Don't forget to add your user to the `docker` group if you want
        to run the container as non-root:
        `sudo groupadd docker && sudo usermod -aG docker $USER`
        (you will need to log off for this change to exist)
1. Change directory at the root of this repository
1. Build the docker image: `docker build -t pfia .`. Be advised,
   building the image may take several minutes depending on your internet
   speed and your machine.

#### Run the tutorial environment
Type `docker run -tp 8888:8888 pfia` to launch the image.
Then click on one of the URL on your terminal, it
will open a window in your web browser. Double-click on "tutorial.ipynb" and
you are good to go :)

### Install dependencies manually

* python dependencies:
  `pip install -r requirements.txt && pip install jupyter`
* ISAIEH:
  * install the ocaml opam build system
  [available here](https://opam.ocaml.org/doc/Install.html)
  * system dependencies are `m4`, `gmp`, `g++`, `gcc`, `cmake`, `make`
  * install command `git clone https://git.frama-c.com/pub/isaieh.git && cd isaieh && opam init --disable-sandboxing && opam install -y . && eval $(opam env) && dune build`.
* Z3: your linux distribution should have a package (named z3), check
  [here](https://github.com/Z3Prover/z3) for detailed installation instructions
* Marabou: either get the static binary with
  `wget https://aisafety.stanford.edu/marabou/marabou-1.0-x86_64-linux.zip`,
  or compile from [source](https://github.com/NeuralNetworkVerification/Marabou)
* PyRAT comes as a precompiled binary that requires Python 3.7.6

When everything is installed, `jupyter notebook tutorial.ipynb` should
open the tutorial notebook in your web browser.

### Authors
* Julien Girard-Satabin
* Guillaume Charpiat
* Zakaria Chihani
* Augustin Lemesle
* Marc Schoenauer
