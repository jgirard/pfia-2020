FROM alpine:latest AS build
RUN apk update && apk add --no-cache \
gcc \
git \
gmp \
gmp-dev \
libc-dev \
make \
m4 \
ocaml \
ocaml-compiler-libs \
ocaml-ocamldoc \
opam \
perl
RUN git clone https://git.frama-c.com/pub/isaieh.git \
&& cd isaieh \
&& opam init --disable-sandboxing \
&& opam switch create install_switch ocaml-base-compiler.4.11.0 \
&& eval $(opam env) \
&& opam switch install_switch \
&& eval $(opam env) \
&& opam install -y . \
&& eval $(opam env) \
&& dune build

FROM jupyter/base-notebook AS runtime
USER root
WORKDIR .
#Installing dependencies
COPY ./requirements.txt .
RUN pip install -r requirements.txt \
&& apt-get update \
&& apt-get install -y unzip wget z3 \
&& wget https://aisafety.stanford.edu/marabou/marabou-1.0-x86_64-linux.zip \
&& unzip marabou-1.0-x86_64-linux.zip
#Get ISAIEH executable, pyrat, marabou and the utils
#to the container file system
COPY ./tutorial.ipynb .
COPY ./network.onnx .
COPY ./network.nnet .
COPY ./formula_lang.py .
COPY ./pyrat.pyc .
COPY ./doc/imgs/network.png .
COPY ./doc/imgs/problem_small.png .
COPY --from=build isaieh/_build/default/bin/converter_static/converter_static.exe ./isaieh.exe
#Exposing port 8888, which is the default for Jupyter
EXPOSE 8888
#Execute jupyter notebook
CMD ["jupyter","notebook","tutorial.ipynb","--allow-root"]
